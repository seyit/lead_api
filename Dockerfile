FROM python:3.6-slim-buster

# Ensure that Python outputs everything that's printed inside
# the application rather than buffering it
ENV PYTHONUNBUFFERED 1

RUN apt update && apt install -y --no-install-recommends \
        build-essential \
        gcc \
        make \
        gettext \
    && rm -rf /var/lib/apt/lists/*

COPY . /lead_api/
WORKDIR /lead_api

RUN pip install --upgrade pip setuptools
RUN pip install --no-cache -r requirements.txt uwsgi

RUN python manage.py collectstatic




