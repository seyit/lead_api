from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from customers.models import Customer
from customers.serializers import CustomerSerializer


class HealthCheck(APIView):
    
    def get(self, request):
        content = {'message': 'success'}
        return Response(content)


class AuthCheck(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        content = {'message': 'success'}
        return Response(content)


class CustomerViewSet(ModelViewSet):
    serializer_class = CustomerSerializer
    queryset = Customer.objects.all()
    permission_classes = (IsAuthenticated,)

    def create(self, request, *args, **kwargs):
        data = request.data
        serializer = self.get_serializer(data=data)
        is_valid = serializer.is_valid()

        if is_valid:
            (obj, created) = Customer.objects.get_or_create(
                first_name=serializer.validated_data["first_name"],
                last_name=serializer.validated_data["last_name"],
                email=serializer.validated_data["email"],
                company_name=serializer.validated_data["company_name"],
                headcount=serializer.validated_data["headcount"]
            )

            return Response({"message": "object created", "id": str(obj.id)})

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



