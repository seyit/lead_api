**Deployment Requirements**

- Fill the DB credentials in docker-compose.yml
- Connect with docker exec to migrate for DB and create superuser for Django

**API endpoint for GET and POST, customer information**
> /customers/ 

Require Token in request header for Authentication. Tokens are created by admin.  
> Authorization Token _APITOKEN_

To get token via API, use endpoint below with username and password in POST data (in json format).
> /api_token/