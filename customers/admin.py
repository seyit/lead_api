from django.contrib import admin
from customers.models import Customer


class CustomerAdmin(admin.ModelAdmin):
    list_display = (
                    'first_name',
                    'last_name',
                    'email',
                    'company_name',
                    'headcount',
                    'notes',
                    'creation_timestamp',
                    'updated_timestamp',
                    )


admin.site.register(Customer, CustomerAdmin)
