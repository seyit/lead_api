from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator

from customers.models import Customer


class CustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = (
            "id",
            "first_name",
            "last_name",
            "email",
            "company_name",
            "headcount",
            "notes",
            "creation_timestamp",
            "updated_timestamp",
        )
        extra_kwargs = {
            "creation_timestamp": {"read_only": True},
            "lastmod_timestamp": {"read_only": True},
            "email": {"required": True},
        }
        validators = [
            UniqueTogetherValidator(
                queryset=Customer.objects.all(),
                fields=("email", "first_name", "last_name"),
            )
        ]