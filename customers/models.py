from django.db import models
from django.utils.translation import gettext_lazy as _


class Customer(models.Model):
    first_name = models.CharField(_('first name'), max_length=100, blank=True)
    last_name = models.CharField(_('last name'), max_length=100, blank=True)
    email = models.EmailField(_('email address'), max_length=100, blank=True)
    company_name = models.CharField(_('company name'), max_length=100, blank=True, null=True)
    headcount = models.IntegerField(_('number of employee'), blank=True, null=True)
    notes = models.TextField(blank=True)

    creation_timestamp = models.DateTimeField(auto_now_add=True)
    updated_timestamp = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = ('email', 'first_name', 'last_name')
